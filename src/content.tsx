import * as React from "react";
import * as ReactDOM from "react-dom";
import { MemoryRouter as Router, Route } from "react-router-dom";
import Home from "./screens/app/Home";
import Success from "./screens/app/Success";
import Tags from "./screens/app/Tags";
import Header from "./components/Header";
import "./styles/app.scss";

function App() {
	return (
		<>
			<Header />
			<Router initialEntries={["/home", "/tags", "/success"]} initialIndex={0}>
				<Route exact path="/home" component={Home} />
				<Route exact path="/tags" component={Tags} />
				<Route exact path="/success" component={Success} />
			</Router>
		</>
	);
}

(function () {
	let pageContainerNode = getPageContainerNode();
	let sidepanelNode = document.createElement("div");

	sidepanelNode.style.width = "300px";
	sidepanelNode.id = "timetackle_sidepanel";
	pageContainerNode.id = "timetackle_page-container";
	pageContainerNode.append(sidepanelNode);

	ReactDOM.render(<App />, document.getElementById("timetackle_sidepanel"));
})();

function getPageContainerNode(): any {
	let containerNode =
		document.getElementById("YPCqFe")!.parentNode!.parentNode!.parentNode;
	return containerNode;
}
