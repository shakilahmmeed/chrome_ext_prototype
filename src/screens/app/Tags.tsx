import * as React from "react";
import addIcon from "../../images/add.png";
import deleteIcon from "../../images/delete.png";

export default function Tags() {
	return (
		<>
			<div id="timetackle_container" className="container">
				<h2>TimeTackle Tags</h2>
				<div id="addedTagsContainer">
					{addedTags.map((tag, idx) => (
						<div key={idx}>
							<form className="addedRow">
								<input className="tagKey" value={tag.key} disabled />
								<input className="tagValue" value={tag.value} disabled />
								<button className="trash">
									<img src={deleteIcon} alt="trash" />
								</button>
							</form>
						</div>
					))}
				</div>
				<div id="eventHasNoTagMsg" className="warnMsg">
					<p>This event has no tags yet.</p>
				</div>
				<div id="addTagsContainer">
					<h3 className="add-tag-title">Add New Tag</h3>
					<div>
						<form>
							<select className="tagKey">
								<option value="Sales">Sales</option>
							</select>
							<input className="tagValue" disabled />
							<button className="add">
								<img src={addIcon} alt="add" />
							</button>
						</form>
					</div>
				</div>
				<div className="identity-label">
					Signed in with{" "}
					<strong className="user-email">mrtester102@gmail.com</strong>
				</div>
			</div>
			<style jsx>{`
				.container {
					margin-top: 25px;
					position: relative;
					height: 100%;
				}

				#eventHasNoTagMsg {
					display: none;
				}

				.trash img,
				.add img {
					width: 16px;
					height: auto;
				}

				.identity-label {
					text-align: center;
					position: absolute;
					bottom: 102px;
					font-size: 12px;
					width: 100%;
				}

				.user-email {
					white-space: nowrap;
				}
			`}</style>
		</>
	);
}

const addedTags = [
	{ key: "IT", value: "Yes" },
	{ key: "Marketing", value: "Yes" },
];
