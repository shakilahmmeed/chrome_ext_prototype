import * as React from "react";
import { Link } from "react-router-dom";
import tags from "../../images/tags.png";

const App = () => {
	return (
		<>
			<div className="container">
				<div className="contents">
					<img src={tags} alt="tags" />
					<h3>Sign in with your TimeTackle email</h3>
					<Link
						to="/success"
						// href="https://app.timetackle.com/authentication"
						// target="_blank"
						className="btn btn-filled"
					>
						Login
					</Link>
				</div>
			</div>

			<style jsx>{`
				.container {
					text-align: center;
					display: flex;
					justify-content: center;
					align-items: center;
					height: 100%;
				}

				.contents {
					margin-top: -92px;
				}

				img {
					width: 180px;
					margin-bottom: 30px;
				}

				a {
					margin-top: 25px;
				}
			`}</style>
		</>
	);
};

export default App;
