import * as React from "react";
import { Link } from "react-router-dom";
import successIcon from "../../images/success.png";

export default function Success() {
	return (
		<>
			<div className="container">
				<div className="contents">
					<img src={successIcon} alt="success-img" className="success-icon" />
					<h3 className="message">You are successfully logged in</h3>
					<div className="btn-group">
						<button className="btn btn-filled" id="hide-ext">
							Hide This Panel
						</button>
						<Link
							to="/tags"
							// href="https://www.youtube.com/watch?v=2EMBihoU4QA"
							// target="_blank"
							className="btn btn-outlined"
						>
							Watch the How to Video
						</Link>
					</div>
				</div>
			</div>

			<style jsx>{`
				.container {
					text-align: center;
					display: flex;
					justify-content: center;
					align-items: center;
					height: 100%;
				}

				.contents {
					margin-top: -92px;
				}

				img {
					opacity: 0.85;
					width: 100px;
					height: auto;
					object-fit: contain;
				}

				.btn-group {
					margin-top: 25px;
					width: 100%;
				}

				a {
					margin-top: 15px;
				}
			`}</style>
		</>
	);
}
