import * as React from "react";
import logo from "../../images/logo.png";

export default function index() {
	return (
		<div className="popup-container">
			<div id="header">
				<div className="logo">
					<img src={logo} alt="logo" />
				</div>
				<h2>
					TimeTackle <span>for Chrome</span>
				</h2>
			</div>
			<div id="buttons">
				<button id="login">Login to your account</button>

				<button id="reloadTags">Reload tags</button>
				<br />

				<button id="logout">Logout</button>

				<div className="hr"></div>

				<input type="checkbox" id="showAtSidePanel" name="showAtSidePanel" />
				<label htmlFor="showAtSidePanel">Show as side panel</label>
				<br />
			</div>
		</div>
	);
}
