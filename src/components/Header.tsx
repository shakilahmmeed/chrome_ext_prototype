import * as React from "react";
import logoLarge from "../images/logo-lg.png";

export default function Header() {
	return (
		<>
			<div className="img-wrapper">
				<img src={logoLarge} alt="brand logo" />
			</div>

			<style jsx>{`
				.img-wrapper {
					width: auto;
					height: 75px;
					margin: 0px auto;
					margin-top: -11px;
					border-bottom: 1px solid #ccc;
					display: flex;
					justify-content: center;
					align-items: center;
				}

				img {
					width: 150px;
					height: auto;
					margin-top: 9px;
				}
			`}</style>
		</>
	);
}
