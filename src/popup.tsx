import * as React from "react";
import * as ReactDOM from "react-dom";
import Popup from "./screens/popup";
import "./styles/popup.scss";

var mountNode = document.getElementById("popup");
ReactDOM.render(<Popup />, mountNode);
